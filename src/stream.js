const LOG_WRAP_LIMIT = 79

// Join any lines which look like they have wrapped.
async function* concatConsecutiveLines(stream) {
  let buffer = []

  function _flush() {
    const t = buffer.join('')
    buffer.length = 0
    return t
  }

  let prevLine = null
  for await (const line of stream) {
    if (prevLine !== null &&
        // If the previous line is as long as the wrap limit then
        // append this line to it.
        // Some lines end with ... when LaTeX knows it's hit the limit
        // These shouldn't be wrapped.
        !(prevLine.length === LOG_WRAP_LIMIT &&
         prevLine.slice(-3) !== '...')) {
      yield _flush()
    }
    buffer.push(line)
    prevLine = line
  }
  if (buffer.length) yield _flush()
}

async function* toLines(stream) {
  // uncompleted line
  let buffer = []

  function _flush() {
    const t = buffer.join('')
    buffer.length = 0
    return t.slice(-1) === '\r' ? t.slice(0, -1) : t
  }

  for await (const _chunk of stream) {
    let chunk = _chunk
    let pos
    while ((pos = chunk.indexOf('\n')) >= 0) {
      buffer.push(chunk.slice(0, pos))
      yield _flush()
      // skip the NL character
      chunk = chunk.slice(pos + 1)
    }
    if (chunk.length) buffer.push(chunk)
  }
  if (buffer.length) yield _flush()
}

export class LogText {
  constructor(stream) {
    this.lineStream = concatConsecutiveLines(toLines(stream))
    this.lines = []
    this.streamEnded = false
    this.row = 0
  }

  async nextLine() {
    this.row++

    if (!this.streamEnded) {
      // populate lines if possible
      for (let i = this.lines.length; this.row >= i; i++) {
        const {value, done} = await this.lineStream.next()
        if (done) {
          break
        } else {
          this.lines.push(value)
        }
      }
    }

    return this.row >= this.lines.length ? false : this.lines[this.row]
  }

  rewindLine() {
    this.row--
  }

  linesUpToNextWhitespaceLine() {
    return this.linesUpToNextMatchingLine(/^ *$/)
  }

  async linesUpToNextMatchingLine(match) {
    const lines = []

    while (true) {
      const nextLine = await this.nextLine()

      if (nextLine === false) {
        break
      }

      lines.push(nextLine)

      if (nextLine.match(match)) {
        break
      }
    }

    return lines
  }
}
