import { LogText } from './stream.js'

// Define some constants
const LATEX_WARNING_REGEX = /^LaTeX(?:3| Font)? Warning: (.*)$/
const BADBOX_WARNING_REGEX = /^(Over|Under)full \\(v|h)box/
const PACKAGE_WARNING_REGEX = /^((?:Package|Class|Module) \b.+\b Warning:.*)$/
// This is used to parse the line number from common latex warnings
const LINES_REGEX = /lines? ([0-9]+)/
// This is used to parse the package name from the package warnings
const PACKAGE_REGEX = /^(?:Package|Class|Module) (\b.+\b) Warning/
const FILE_LINE_ERROR_REGEX = /^([./].*):(\d+): (.*)/
const INFO_REGEX = /^(\S+) written on (.+)$/

const LATEX_WARNING_REGEX_OLD = /^LaTeX Warning: (.*)$/
const PACKAGE_WARNING_REGEX_OLD = /^(Package \b.+\b Warning:.*)$/
const PACKAGE_REGEX_OLD = /^Package (\b.+\b) Warning/

const STATE = {
  NORMAL: 0,
  ERROR: 1,
}

export default class LatexParser {
  constructor(stream, options = {}) {
    this.state = STATE.NORMAL
    this.verbose = options.verbose
    this.fileBaseNames = options.fileBaseNames || [/compiles/, /\/usr\/local/]
    this.ignoreDuplicates = options.ignoreDuplicates
    this.data = []
    this.fileStack = []
    this.currentFileList = this.rootFileList = []
    this.openParens = 0
    // TODO: Needed only for beta release; remove when over. 20220530
    if (options.oldRegexes) {
      this.latexWarningRegex = LATEX_WARNING_REGEX_OLD
      this.packageWarningRegex = PACKAGE_WARNING_REGEX_OLD
      this.packageRegex = PACKAGE_REGEX_OLD
    } else {
      this.latexWarningRegex = LATEX_WARNING_REGEX
      this.packageWarningRegex = PACKAGE_WARNING_REGEX
      this.packageRegex = PACKAGE_REGEX
    }
    this.log = new LogText(stream)
  }

  emitLogEntry(data) {
    if (this.parseCallback) {
      this.parseCallback(data)
    }
    this.data.push(data)
  }

  async parse(parseCallback) {
    this.parseCallback = parseCallback

    while ((this.currentLine = await this.log.nextLine()) !== false) {
      if (this.verbose) {
        console.warn(`Read a line: [${this.currentLine}]`)
      }

      if (this.state === STATE.NORMAL) {
        if (this.currentLineIsError()) {
          this.state = STATE.ERROR
          this.currentError = {
            line: null,
            file: this.currentFilePath,
            level: 'error',
            message: this.currentLine.slice(2),
            content: '',
            raw: this.currentLine + '\n',
          }
        } else if (this.currentLineIsFileLineError()) {
          this.state = STATE.ERROR
          this.parseFileLineError()
        } else if (this.currentLineIsRunawayArgument()) {
          this.emitLogEntry(await this.asyncParseRunawayArgumentError())
        } else if (this.currentLineIsWarning()) {
          // FIXME: Some LaTeX errors are not single-lined, and these will be truncated, e.g.,
          // LaTeX Warning: You have requested document class `foo/bar',
          //                but the document class provides `bar'.
          // LaTeX Font Warning: Size substitutions with differences
          // (Font)              up to 1.28pt have occurred.
          this.emitLogEntry(this.parseSingleWarningLine(this.latexWarningRegex))
        } else if (this.currentLineIsBadBoxWarning()) {
          this.emitLogEntry(this.parseBadBoxLine())
        } else if (this.currentLineIsPackageWarning()) {
          this.emitLogEntry(await this.asyncParseMultipleWarningLine())
        } else if (this.currentLineIsInfo()) {
          this.emitLogEntry(this.parseInfoLine())
        } else {
          this.stripsParensForFilenames()
          if (this.verbose && this.currentLine.length) {
            this.emitLogEntry({
              line: null,
              file: this.currentFilePath ?? null,
              level: 'debug',
              message: 'Unrecognized message: ' + this.currentLine,
              raw: this.currentLine + '\n',
            })
          }
        }
      }
      if (this.state === STATE.ERROR) {
        this.currentError.content += (
          await this.log.linesUpToNextMatchingLine(/^l\.[0-9]+/))
          .join('\n')
        this.currentError.content += '\n'
        this.currentError.content += (
          await this.log.linesUpToNextWhitespaceLine())
          .join('\n')
        this.currentError.content += '\n'
        this.currentError.content += (
          await this.log.linesUpToNextWhitespaceLine())
          .join('\n')
        this.currentError.raw += this.currentError.content
        const lineNo = this.currentError.raw.match(/l\.([0-9]+)/)
        if (lineNo && this.currentError.line === null) {
          this.currentError.line = parseInt(lineNo[1], 10)
        }
        this.emitLogEntry(this.currentError)
        this.state = STATE.NORMAL
      }
    }

    this.parseCallback = null

    return this.postProcess(this.data)
  }

  currentLineIsError() {
    return (
      this.currentLine[0] === '!' &&
      this.currentLine !==
        '!  ==> Fatal error occurred, no output PDF file produced!'
    )
  }

  currentLineIsFileLineError() {
    return FILE_LINE_ERROR_REGEX.test(this.currentLine)
  }

  currentLineIsRunawayArgument() {
    return this.currentLine.match(/^Runaway argument/)
  }

  currentLineIsWarning() {
    return !!this.currentLine.match(this.latexWarningRegex)
  }

  currentLineIsPackageWarning() {
    return !!this.currentLine.match(this.packageWarningRegex)
  }

  currentLineIsBadBoxWarning() {
    return !!this.currentLine.match(BADBOX_WARNING_REGEX)
  }

  currentLineIsInfo() {
    return !!this.currentLine.match(INFO_REGEX)
  }

  parseFileLineError() {
    const result = this.currentLine.match(FILE_LINE_ERROR_REGEX)
    this.currentError = {
      line: result[2],
      file: result[1],
      level: 'error',
      message: result[3],
      content: '',
      raw: this.currentLine + '\n',
    }
  }

  async asyncParseRunawayArgumentError() {
    this.currentError = {
      line: null,
      file: this.currentFilePath,
      level: 'error',
      message: this.currentLine,
      content: '',
      raw: this.currentLine + '\n',
    }
    this.currentError.content += (
      await this.log.linesUpToNextWhitespaceLine())
      .join('\n')
    this.currentError.content += '\n'
    this.currentError.content += (
      await this.log.linesUpToNextWhitespaceLine())
      .join('\n')
    this.currentError.raw += this.currentError.content
    const lineNo = this.currentError.raw.match(/l\.([0-9]+)/)
    if (lineNo) {
      this.currentError.line = parseInt(lineNo[1], 10)
    }
    return this.currentError
  }

  parseSingleWarningLine(prefixRegex) {
    const warningMatch = this.currentLine.match(prefixRegex)
    if (!warningMatch) {
      return
    }
    const warning = warningMatch[1]
    const lineMatch = warning.match(LINES_REGEX)
    const line = lineMatch ? parseInt(lineMatch[1], 10) : null
    return {
      line,
      file: this.currentFilePath,
      level: 'warning',
      message: warning,
      raw: warning,
    }
  }

  async asyncParseMultipleWarningLine() {
    // Some package warnings are multiple lines, let's parse the first line
    let warningMatch = this.currentLine.match(this.packageWarningRegex)
    // Something strange happened, return early
    if (!warningMatch) {
      return
    }
    const warningLines = [warningMatch[1]]
    let lineMatch = this.currentLine.match(LINES_REGEX)
    let line = lineMatch ? parseInt(lineMatch[1], 10) : null
    const packageMatch = this.currentLine.match(this.packageRegex)
    const packageName = packageMatch[1]
    // Regex to get rid of the unnecesary (packagename) prefix in most multi-line warnings
    const prefixRegex = new RegExp(
      '(?:\\(' + packageName + '\\))*[\\s]*(.*)',
      'i'
    )
    // After every warning message there's a blank line, let's use it
    while ((this.currentLine = await this.log.nextLine())) {
      lineMatch = this.currentLine.match(LINES_REGEX)
      line = lineMatch ? parseInt(lineMatch[1], 10) : line
      warningMatch = this.currentLine.match(prefixRegex)
      warningLines.push(warningMatch[1])
    }
    const rawMessage = warningLines.join(' ')
    return {
      line,
      file: this.currentFilePath,
      level: 'warning',
      message: rawMessage,
      raw: rawMessage,
    }
  }

  parseBadBoxLine() {
    const lineMatch = this.currentLine.match(LINES_REGEX)
    const line = lineMatch ? parseInt(lineMatch[1], 10) : null
    return {
      line,
      file: this.currentFilePath,
      level: 'typesetting',
      message: this.currentLine,
      raw: this.currentLine,
    }
  }

  parseInfoLine() {
    const matched = this.currentLine.match(INFO_REGEX)
    return {
      line: null,
      file: this.currentFilePath,
      level: 'info',
      message: this.currentLine,
      raw: this.currentLine,
    }
  }

  // Check if we're entering or leaving a new file in this line

  stripsParensForFilenames() {
    const pos = this.currentLine.search(/\(|\)/)
    if (pos !== -1) {
      const token = this.currentLine[pos]
      this.currentLine = this.currentLine.slice(pos + 1)
      if (token === '(') {
        const filePath = this.consumeFilePath()
        if (filePath) {
          this.currentFilePath = filePath
          const newFile = {
            path: filePath,
            files: [],
          }
          this.fileStack.push(newFile)
          this.currentFileList.push(newFile)
          this.currentFileList = newFile.files
        } else {
          this.openParens++
        }
      } else if (token === ')') {
        if (this.openParens > 0) {
          this.openParens--
        } else {
          if (this.fileStack.length > 1) {
            this.fileStack.pop()
            const previousFile = this.fileStack[this.fileStack.length - 1]
            this.currentFilePath = previousFile.path
            this.currentFileList = previousFile.files
          }
        }
      }
      // else {
      //         Something has gone wrong but all we can do now is ignore it :(
      // }
      // Process the rest of the line
      this.stripsParensForFilenames()
    }
  }

  consumeFilePath() {
    // Our heuristic for detecting file names are rather crude
    // A file may not contain a ')' in it
    // To be a file path it must have at least one /
    if (!this.currentLine.match(/^\/?([^ )]+\/)+/)) {
      return false
    }
    let endOfFilePath = this.currentLine.search(/ |\)/)

    // handle the case where there is a space in a filename
    while (endOfFilePath !== -1 && this.currentLine[endOfFilePath] === ' ') {
      const partialPath = this.currentLine.slice(0, endOfFilePath)
      // consider the file matching done if the space is preceded by a file extension (e.g. ".tex")
      if (/\.\w+$/.test(partialPath)) {
        break
      }
      // advance to next space or ) or end of line
      const remainingPath = this.currentLine.slice(endOfFilePath + 1)
      // consider file matching done if current path is followed by any of "()[]
      if (/^\s*["()[\]]/.test(remainingPath)) {
        break
      }
      const nextEndOfPath = remainingPath.search(/[ "()[\]]/)
      if (nextEndOfPath === -1) {
        endOfFilePath = -1
      } else {
        endOfFilePath += nextEndOfPath + 1
      }
    }
    let path
    if (endOfFilePath === -1) {
      path = this.currentLine
      this.currentLine = ''
    } else {
      path = this.currentLine.slice(0, endOfFilePath)
      this.currentLine = this.currentLine.slice(endOfFilePath)
    }
    return path
  }

  postProcess(data) {
    const all = []
    const errorsByLevel = {
      error: [],
      warning: [],
      typesetting: [],
      info: [],
    }
    const hashes = new Set()

    const hashEntry = entry => entry.raw

    data.forEach(item => {
      const hash = hashEntry(item)

      if (this.ignoreDuplicates && hashes.has(hash)) {
        return
      }

      errorsByLevel[item.level]?.push(item)

      all.push(item)
      hashes.add(hash)
    })

    return {
      errors: errorsByLevel.error,
      warnings: errorsByLevel.warning,
      typesetting: errorsByLevel.typesetting,
      info: errorsByLevel.info,
      all,
      files: this.rootFileList,
    }
  }
}
