# latex-log-parser-async

This is an enhanced version of Overleaf's latex-log-parser. The idea is to make everything asynchronous so that one can pipe a LaTeX program's output to it and expect it to be transformed in real time.

The original code can be found at:
  * https://github.com/overleaf/overleaf/blob/main/services/web/frontend/js/ide/log-parser/latex-log-parser.js
  * https://github.com/overleaf/overleaf/blob/main/services/web/test/frontend/ide/log-parser/logParserTests.js

Sample usage:

```js
import { LatexParser } from 'latex-log-parser-async'

;(async () => {
  process.stdin.setEncoding('utf-8')
  const Parser = new LatexParser(process.stdin, {})
  const result = await Parser.parse(({line, file, level, message}) => {
    const loc = file + (line ? `:${line}` : '')
    console.log(`[${level}] ${loc}: ${message}`)
  })

  if (result.errors.length) {
    console.log('Compilation failed...')
  } else {
    console.log('Done!')
  }
})()
```
